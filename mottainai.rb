#!/usr/bin/env ruby
#
# AWS のアクセスキーは環境変数から与えます。
# 下記の2つの環境変数をセットしてください。
#       AWS_SECRET_ACCESS_KEY
#       AWS_ACCESS_KEY_ID

require 'bundler'
Bundler.require

require 'pry'
require 'optparse'

# コマンドライン引数をパース
opts = {
  region: 'ap-northeast-1' # リージョン指定がない場合は東京
}
OptionParser.new {|opt|
  # インスタンスIDを指定 (複数可)
  opt.on('-i INSTANCEID', '--instance-id INSTANCEID') {|v|
    if opts[:instances].is_a? Array
      opts[:instances] << v
    else
      opts[:instances] = [v]
    end
  }

  # アクションを指定
  opt.on('-a ACTION', '--action ACTION') {|v|
    opts[:action] = v.to_sym
  }

  # リージョンを指定
  opt.on('-r [REGION]', '--region [REGION]') {|v|
    opts[:region] = v
  }

  opt.parse!(ARGV)
}

ec2 = AWS.ec2(region: opts[:region])

# 引数が足りない場合は pry でデバッグモードに入る
binding.pry if opts[:action].nil? || opts[:instances].nil?

# 与えられたインスタンスに対してアクションを実行する
opts[:instances].each do |instance_id|
  ec2.instances[instance_id].send(opts[:action])
end

